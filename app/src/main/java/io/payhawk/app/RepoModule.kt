package io.payhawk.app

import dagger.Binds
import dagger.Module
import io.payhawk.app.repo.Transactions
import io.payhawk.app.repo.Users

@Module
abstract class RepoModule {
    @Binds
    abstract fun provideUsers(users: Users): Users

    @Binds
    abstract fun provideTransactions(transactions: Transactions): Transactions
}