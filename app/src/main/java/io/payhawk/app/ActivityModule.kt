package io.payhawk.app

import android.content.Context
import dagger.Module
import dagger.Provides
import io.payhawk.app.BaseActivity
import io.payhawk.app.utils.inject.ActivityContext
import io.payhawk.app.utils.inject.ActivityScope


@Module
class ActivityModule(private val activity: BaseActivity) {

    @Provides
    @ActivityScope
    fun provideActivity(): BaseActivity {
        return activity
    }

    @Provides
    @ActivityScope
    @ActivityContext
    fun provideContext(): Context {
        return activity
    }
}
