package io.payhawk.app.viewmodels

import android.arch.lifecycle.ViewModel
import io.payhawk.app.models.User
import io.payhawk.app.repo.Users
import javax.inject.Inject


class MainViewModel @Inject constructor(users: Users) : ViewModel() {
    var user: User = users.get()
}