package io.payhawk.app

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.payhawk.app.utils.inject.HasComponent
import javax.inject.Inject

open class BaseActivity : AppCompatActivity(), HasComponent<ActivityComponent> {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    
    private lateinit var _component: ActivityComponent

    override val component: ActivityComponent
        get() = _component

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _component = DaggerActivityComponent.builder()
                .applicationComponent((this.application as MyApplication).component)
                .activityModule(ActivityModule(this))
                .build()
    }

    protected inline fun <reified T : ViewModel> getViewModel(): T =
            ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}