package io.payhawk.app

class Constants{
    companion object {
        val LOGIN_ACTIVITY_REQUEST_CODE = 100
        val CAMERA_ACTIVITY_REQUEST_CODE = 200
        val USER_LOGGED = 1000
    }
}