package io.payhawk.app.utils.inject

interface HasComponent<C> {
    val component: C
}