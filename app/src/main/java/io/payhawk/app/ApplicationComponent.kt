package io.payhawk.app

import dagger.Component
import io.payhawk.app.repo.Users
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, RepoModule::class])
interface ApplicationComponent {
    fun inject(application: MyApplication)
}