package io.payhawk.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.View
import io.payhawk.app.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        viewModel = getViewModel()

        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        camera.setOnClickListener(mOnCameraListener)

        message.text = viewModel.user.name
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.LOGIN_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
//                    setContentView(R.layout.activity_main)
                }
            }
            Constants.CAMERA_ACTIVITY_REQUEST_CODE -> {

            }
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_transactions -> {
                message.setText(R.string.title_transactions)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_cards -> {
                message.setText(R.string.title_cards)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private val mOnCameraListener = View.OnClickListener {
        val intent = Intent("android.media.action.IMAGE_CAPTURE")
        startActivityForResult(intent, Constants.CAMERA_ACTIVITY_REQUEST_CODE)
    }
}
