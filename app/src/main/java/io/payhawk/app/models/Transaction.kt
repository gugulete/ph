package io.payhawk.app.models

import javax.inject.Inject


class Transaction @Inject constructor(val amount: Double)