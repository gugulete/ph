package io.payhawk.app

import dagger.Module
import dagger.Provides
import io.payhawk.app.utils.inject.ApplicationContext
import javax.inject.Singleton


@Module
class ApplicationModule(application: MyApplication) {
    private val application = application

    @Provides
    @Singleton
    @ApplicationContext
    fun providesApplication(): MyApplication {
        return this.application
    }
}
