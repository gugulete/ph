package io.payhawk.app.repo

import io.payhawk.app.models.Transaction
import javax.inject.Inject

class Transactions @Inject constructor() {
    fun get(): Array<Transaction> {
        return Array(3) { Transaction(it * 10.0) }
    }
}


