package io.payhawk.app

import dagger.Component
import io.payhawk.app.utils.inject.ActivityScope
import io.payhawk.app.viewmodels.ViewModelModule

@ActivityScope
@Component(
        dependencies = [ApplicationComponent::class],
        modules = [ActivityModule::class, ViewModelModule::class])
interface ActivityComponent {
    fun inject(app: MainActivity)
}