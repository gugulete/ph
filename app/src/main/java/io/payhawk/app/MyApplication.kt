package io.payhawk.app

import android.app.Application
import io.payhawk.app.utils.inject.HasComponent

class MyApplication : Application(), HasComponent<ApplicationComponent> {
    private lateinit var _component: ApplicationComponent

    override val component: ApplicationComponent
        get() = _component


    override fun onCreate() {
        super.onCreate()

        _component = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }
}